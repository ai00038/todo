/**
	start project
*/

// services module
var servicesModule = angular.module('servicesModule',[]);

/** controllers module */
var controllersModule = angular.module('controllersModule',['servicesModule']);



// app module.. dependent on controllers
var app = angular.module('MyToDoList', [
						'controllersModule',
						'ngRoute','ngResource', 'ngMessages'
	]).config( function ($routeProvider){// config for ng route... how it needs to route for diff pages.. navigation within spa
			//essentially changing views here
		$routeProvider
			.when('/', {
				templateUrl : 'app/views/home.html'
			})
			.when('/todo', {
				templateUrl : 'app/views/todo.html',
				controller : 'MainController'
			}).when('/todo/:taskId',{
				templateUrl : 'app/views/ai-todo-indiv.html',
				controller : 'IndivController',
			})
			.when('/daily' ,{
				templateUrl : 'app/views/daily.html',
				controller : 'DailyController'
			}).when('/filter' , {
				templateUrl : 'app/views/todo-filters.html',
				controller : 'TodoFilterController'
			})

	}
);


