app.directive('aiTodoItem' , function () {
	return {
		restrict : 'A',
		templateUrl: 'app/templates/ai-todo-item.html'
	};
});