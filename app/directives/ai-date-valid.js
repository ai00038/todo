app.directive('aiDateValid' , function () {
	return {
		require: 'ngModel', // this element allows directive to interact with the controller of the page
		restrict : 'A',
		    link: function(scope, elm, attrs, ctrl) { // link - allows directive to interact with DOM of page... 
		    											// scope - angular scope object
		    											// elm - jqlite element the element matches
		    											// attrs - is a hash object with key-value pairs of normalized attribute names and their corresponding attribute values. 
		     											// ctrl -  Any controllers used in required
		      ctrl.$parsers.unshift(function(viewValue) {
		      	var today = new Date();
		      	today.setHours(0,0,0,0);// to the start of today
		      	console.log(viewValue);
		      	var deadline = new Date(viewValue);
		      	var limit = new Date('2100-01-01');

		      	var valid = true;
		       
		        if ( deadline < today) {
		          valid = false;
		          ctrl.$setValidity('pastDate', false)
		        } 

		        if ( deadline >= limit) { 
		          valid = false;
		          ctrl.$setValidity('futureLimit', false);
		        } 

		       	if(!valid) {
		       		return valid;
		       	}


		          ctrl.$setValidity('pastDate', true)

		          ctrl.$setValidity('futureLimit', true);

		        return viewValue;
		        	
		      });
    		}


	};
});

/////////////////////////example

/*
var INTEGER_REGEXP = /^\-?\d*$/;
app.directive('integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (INTEGER_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('integer', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('integer', false);
          return undefined;
        }
      });
    }
  };
});
*/