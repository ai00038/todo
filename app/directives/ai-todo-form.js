app.directive('aiTodoForm' , function () {
	return {
		restrict : 'E',
		templateUrl: '/app/templates/ai-todo-form.html'
	};
});