app.directive('aiTodoNav' , function () {
	return {
		restrict : 'A',
		templateUrl: '/app/templates/ai-todo-nav.html'
	};
});