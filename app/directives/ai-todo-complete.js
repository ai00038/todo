app.directive('aiTodoComplete' , function () {
	return {
		restrict : 'A',
		templateUrl: '/app/templates/ai-todo-complete.html'
	};
});