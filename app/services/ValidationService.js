"use strict";

app.service('ValidationService', function() {

	return {

		assertNotNEU : function (input) { // throwing an error if an input is null, empty or undefined

			if(input === null || input === "" || input === undefined){
				throw new Error("Input cannot be Null, Empty or Undefined");
			}

		}
		
	}


});