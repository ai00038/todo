"use strict";

servicesModule.service('thisDailyService' , function () {
	var dailyList = {

		latestId:0,
		dailytasks : {}

	};

	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllTasks : function () { // return all objs
			return objectToArray(dailyList.dailytasks);
		},

		add : function (daily) {
			var id = dailyList.latestId + 1; // increment latestId
			dailyList.latestId = id; // update incremented id
			daily.id = id; // give task latest id
			dailyList.dailytasks[id] = daily;
			
			return id;
		},

		update : function (task) {
			dailyList.dailytasks[task.id] = task;
		},

		remove : function (id) {// deletes from backend list?
			delete dailyList.dailytasks[id];
		},

		getById : function (id) {
			return dailyList.dailytasks[id];
		}
	}

});

/**
	to test:
	-getAllTasks
	-add
	-update
	-remove
	-getById
*/