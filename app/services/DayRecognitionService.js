"use strict";

app.service('DayRecognitionService', function() {

	return {
		recognizeDay : function (input) {

			var upper = input.toUpperCase();

			if(upper === "MONDAY"
			|| upper === "TUESDAY"
			|| upper === "WEDNESDAY"
			|| upper === "THURSDAY"
			|| upper === "FRIDAY"
			|| upper === "SATURDAY"
			|| upper === "SUNDAY"
			){
				return upper;
			} else {
				return "NO DEADLINE";
			}

		}
	}


});