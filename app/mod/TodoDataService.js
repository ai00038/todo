"use strict";

/**
	service to handle all data
*/

servicesModule.service('thisToDoService' , function ($resource) {
	var Todo = $resource('http://lapbtn11906:8090/api/todos');

	 return {
		getAllTasks : function () { // return all objs
			return Todo.query();
		},

		add : function (task) {
		},

		update : function (task) {
		},

		remove : function (id) {// deletes from backend list?
			return Todo.delete({id: todoId});
		},

		getById : function (todoId) {
			return Todo.get({id: todoId});
		}
	}

});

/**
	to test:
	-getAllTasks
	-add
	-update
	-remove
	-getById
*/