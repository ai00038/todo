/**
	Class to encapsulate task to do... extends item
*/
"use strict";

var ToDoList = ToDoList || {};

function TaskToDo(title, complete){
	
	
	var _complete; // will be initialised wil false anyway but want to see it...
	
	Object.defineProperty( this, 'complete', {
		
			
		get : function () {
			
			return _complete;
			
		},
		
		set : function (complete) {
		
			_complete = complete;
		
		} 
	
	});
	
	
	Item.call(this, title);

};



// prototype are shared functions... 

// subclass extends superclass
TaskToDo.prototype = Object.create(Item.prototype);
TaskToDo.prototype.constructor = TaskToDo;

TaskToDo.prototype.toggleComplete = function () {
	
	this.complete = !this.complete; // opposite.. assume "complete" is a bool
	
};
