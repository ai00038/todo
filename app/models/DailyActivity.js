/**
	Class to encapsulate daily activity to record in log... extends item.
*/
"use strict";

var ToDoList = ToDoList || {};

/**
	startTime and finishTime are JS Date() objs that have been fully validated
*/
function DailyActivity(title, startTime, finishTime){
	
	var _startTime;
	var _finishTime;
	
	Object.defineProperty( this, 'startTime', {
		
			
		get : function () {
			
			return _startTime;
			
		},
		
		set : function (startTime) {
		
			_startTime = startTime;
		
		} 
	
	});
	
	Object.defineProperty( this, 'finishTime', {
		
			
		get : function () {
			
			return _finishTime;
			
		},
		
		set : function (startTime) {
		
			_finishTime = finishTime;
		
		} 
	
	});
	
	this.finishTime = finishTime;
	this.startTime = startTime;
	
	Item.call(this, title); // the last thing that should be done
};


DailyActivity.prototype = Object.create(Item.prototype);
DailyActivity.prototype.constructor = DailyActivity;
