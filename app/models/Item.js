/**
	Class to encapsulate task
*/
"use strict";

var ToDoList = ToDoList || {};

function Item (title) { // description of task
	var _id;
	var _title;
	// getters and setters for each field defined like this
	Object.defineProperty(this, 'title', {
	
		get : function () {
			
			return _title;
			
		},
		
		set : function (title) {
		
			_title = title;
		
		} 
	
	});
		
	Object.defineProperty(this, 'id', {

		get : function () {
			
			return _id;
			
		},
		
		set : function (id) {
		
			_id = id;
		
		} 
	
	});
	
	this.title = title;
	

	
	
	//Object.preventExtensions(this); // can't add any extensions after implementations
};




