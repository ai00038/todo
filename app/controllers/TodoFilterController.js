"use strict";

angular.module('MyToDoList').controller('TodoFilterController' , ['$scope' ,'thisToDoService', function ($scope, thisToDoService) {


	$scope.toDoList = thisToDoService.getAllTasks();


	$scope.filterComplete = function (task) {

		return (task.complete === true);
	};
	
	$scope.filterIncomplete = function (task) {

		return (task.complete === false);
	};


}]);