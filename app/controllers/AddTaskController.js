"use strict";

/**
	using controller to access "backend" data service's add, update, remove, get by id. ("crud")
*/
angular.module('MyToDoList').controller('AddTaskController' , 
	[ '$scope','thisToDoService', 'ValidationService' , 'DayRecognitionService' , 
		function($scope, thisToDoService, ValidationService, DayRecognitionService) {


			$scope.toDoList = thisToDoService.getAllTasks(); // to array


			$scope.populate = function(task){


				ValidationService.assertNotNEU(task.title);//validate fields
				ValidationService.assertNotNEU(task.details);
				ValidationService.assertNotNEU(task.category);
				//ValidationService.assertNotNEU(task.deadline);


				//task.deadline = DayRecognitionService.recognizeDay(task.deadline);//deadline day recognition

				task.complete = false;


				var id = thisToDoService.add(angular.copy(task));

				if(id){//will either be defined or not...
					task.id = id;
					$scope.toDoList.push(angular.copy(task));
					$scope.task = {};
				}
				
				
			};







 // end controller 
}]);
