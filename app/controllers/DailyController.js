"use strict";

/**
	using controller to access "backend" data service's add, update, remove, get by id. ("crud")

	there's no validation in this system...
*/
controllersModule.controller('DailyController' , [ '$scope', 'thisDailyService' , function($scope, thisDailyService) {


	$scope.dailyList = thisDailyService.getAllTasks(); // to array


	$scope.populate = function(daily){

		var id = thisDailyService.add(angular.copy(daily));

		if(id){//will either be defined or not...
			daily.id = id;
			$scope.dailyList.push(angular.copy(daily));
			$scope.daily = {};
		}
		
		
	};

	$scope.deleteDailyTask = function (id) {

		thisDailyService.remove(id)//delete on "backend"
		$scope.dailyList = thisDailyService.getAllTasks();

	}


}]); // end controller 

/**
	Things to test:
	-populate and deleteDailyTask methods 
*/