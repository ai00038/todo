"use strict";

/**
	using controller to access "backend" data service's add, update, remove, get by id. ("crud")
*/
angular.module('MyToDoList').controller('TodoController' , 
	[ '$scope','thisToDoService', 'ValidationService' , 'DayRecognitionService',
		function($scope, thisToDoService, ValidationService, DayRecognitionService) {


			$scope.toDoList = thisToDoService.getAllTasks(); // to array

			$scope.toggleCompletion = function(task){
				task.complete = !task.complete;
				thisToDoService.update(task);
			};

			$scope.populate = function(task){


				ValidationService.assertNotNEU(task.title);//validate fields
				ValidationService.assertNotNEU(task.details);
				ValidationService.assertNotNEU(task.category);
				//ValidationService.assertNotNEU(task.deadline);


				//task.deadline = DayRecognitionService.recognizeDay(task.deadline);//deadline day recognition

				task.complete = false;


				var id = thisToDoService.add(angular.copy(task));

				if(id){//will either be defined or not...
					task.id = id;
					$scope.toDoList.push(angular.copy(task));
					$scope.task = {};
				}
				
				
			};

			$scope.deleteTask = function (id) {

				thisToDoService.remove(id)//delete on "backend"
				$scope.toDoList = thisToDoService.getAllTasks();

			};

			$scope.editTask = function (task, newTitle, newCategory, newDetails) {// gets task by id, 

				ValidationService.assertNotNEU(newTitle);
				ValidationService.assertNotNEU(newDetails);
				ValidationService.assertNotNEU(newCategory);
				//ValidationService.assertNotNEU(newDeadline);


				task.title = newTitle;
				task.category = newCategory;
				task.details = newDetails;
				//task.deadline = DayRecognitionService.recognizeDay(newDeadline);
				thisToDoService.update(task);
			};






 // end controller 
}]);

/**
	things to test:
	-togglecompletion, populate and deletetask
*/