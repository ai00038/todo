/**
	My first ever gruntfile...
*/

module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.initConfig({

		cssmin : {
			minify : {
				expand :true,
				cwd: 'style/',
				src: ['*.css' , '!*.min.css'],
				dest: 'style',
				ext: '.min.css'
			}
		},

		concat : {

			dist : {

				src: ['style/reset.min.css' , 'style/grid.min.css' , 'style/commonstyle.min.css'],
				dest:'style/all.min.css'

			},

			concatjs : {

				src: ['app/**/*.js', '!app/libraries/*.js'],
				dest:'dist/output.min.js'

			}

		},

		uglify : {

			uglifyJS : {
				files : {
					'dist/output.min.js' : ['app/**/*.js', '!app/libraries/*.js']
				}
			}
 
		},

		watch : {

			css : {

				files : ['style/*.css' , 'style/!*.min.css'],
				tasks : ['cssmin' , 'concat:dist']

			}


		}


	});

	grunt.registerTask('default', ['cssmin']);
	grunt.registerTask('uglifyJS', ['uglify']);
	grunt.registerTask('concatjsfiles',['concat:concatjs']);

}