describe('test directive' , function (){

	var scope,
      directive,
      $compile,
      directiveMarkup;
      
  beforeEach(function (){
    //load the module
    module('MyToDoList');

    module('templates');
    
    //set our view html.
    directiveMarkup = '<div ai-todo-item></div>';
    
    inject(function(_$compile_, $rootScope) {
      //create a scope (you could just use $rootScope, I suppose)
      scope = $rootScope.$new();
      
      $compile = _$compile_;      
    });
  });

  it('Should set the text of the element to whatever was passed.', function() {
   // set a value (the same one we had in the html)
   var compiledElement = $compile(directiveMarkup)(scope);

    scope.task = {
      title : 'Learn Angular',
      details : 'details'

    };
    //check to see if it's blank first.

    scope.$digest(); // digest goes thru html and searches for any expressions

    expect(compiledElement.html()).toContain('Learn Angular');
    expect(compiledElement.html()).toContain('details');
    
    scope.task = {
      title : 'Teach some AngularJS',
      details : 'details'
    };
    scope.$digest();

    expect(compiledElement.html()).not.toContain('Learn Angular');
    expect(compiledElement.html()).toContain('Teach some AngularJS');
    
  
    
    //test to see if it was updated.
 });

});