describe('directives', function() {
  var $scope, form;

  beforeEach(module('MyToDoList'));

  beforeEach(inject(function($compile, $rootScope) {
    $scope = $rootScope;
    var element = angular.element(
      '<form name="form">' +
      '<input  ai-date-valid  type="date" ng-model="model.date" name="date"   />' +
      '</form>'
    );
    $scope.model = { date: null }
    $compile(element)($scope);
    form = $scope.form;
  }));

  describe('date', function() {
    it('should pass with date between now and 1/1/2100', function() {
      form.date.$setViewValue('2088-01-01');
      $scope.$digest();
      expect($scope.model.date).toEqual(new Date('2088-01-01'));
      expect(form.date.$valid).toBe(true);
    });


    it('should not pass with with date in past', function() {
      form.date.$setViewValue('1997-03-18');
      $scope.$digest();
      expect($scope.model.date).toBeUndefined();
      expect(form.date.$valid).toBe(false);
    });


    it('should not pass with date beyond 1/1/2100',function(){
      form.date.$setViewValue('2101-03-18');
      $scope.$digest();
      expect($scope.model.date).toBeUndefined();
      expect(form.date.$valid).toBe(false);
    });


  });
});

 




/*
describe('directives', function() {
  var $scope, form;
  beforeEach(module('exampleDirective'));
  beforeEach(inject(function($compile, $rootScope) {
    $scope = $rootScope;
    var element = angular.element(
      '<form name="form">' +
      '<input ng-model="model.somenum" name="somenum" integer />' +
      '</form>'
    );
    $scope.model = { somenum: null }
    $compile(element)($scope);
    form = $scope.form;
  }));

  describe('integer', function() {
    it('should pass with integer', function() {
      form.somenum.$setViewValue('3');
      $scope.$digest();
      expect($scope.model.somenum).toEqual('3');
      expect(form.somenum.$valid).toBe(true);
    });
    it('should not pass with string', function() {
      form.somenum.$setViewValue('a');
      $scope.$digest();
      expect($scope.model.somenum).toBeUndefined();
      expect(form.somenum.$valid).toBe(false);
    });
  });
});
*/