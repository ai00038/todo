"use strict";

describe('Controller : IndivController' , function (){

	beforeEach(function () {

		module('MyToDoList');

		module(function($provide) { // assign mock service
			$provide.service('thisToDoService', mockToDoDataService);
		});

	});

	var IndivController , scope;

	// init controller and (mock) scope
	beforeEach(inject(function ($controller, $rootScope, _thisToDoService_ ){
		scope = $rootScope.$new();
		IndivController = $controller('IndivController' , {
			$scope: scope,
			thisToDoService : _thisToDoService_
		});

	}));

	it('should return correct name of controller' , function (){
		expect(scope.name).toEqual("IndivController");
	});

	it('should return the only task in list' , function (){
		expect(scope.indivTask).toEqual({

				
				title : "relax",
				details : "sleep",
				complete : false,
				id : 1 

			});
	});


});