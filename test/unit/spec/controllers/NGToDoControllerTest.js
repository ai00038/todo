/**
	To test NGToDoController.js 
*/
"use strict";

describe('Controller : NGToDoController' , function (){

	beforeEach(function () {

		module('MyToDoList');

		module(function($provide) { // assign mock service
			$provide.service('thisToDoService', mockToDoDataService);
		});

	});

	var TodolistController , scope;

	// init controller and (mock) scope
	beforeEach(inject(function ($controller, $rootScope, _thisToDoService_ ){
		scope = $rootScope.$new();
		TodolistController = $controller('TodoController' , {
			$scope: scope,
			thisToDoService : _thisToDoService_
		});

	}));

	it('should initialise list on scope containing todo items' , function (){ // testing getalltasks
		expect(scope.toDoList).toEqual([{
				title : "relax",
				details : "sleep",
				complete : false,

				id : 1 
			}]);
	});

	it('should initialise list with a new todo element appended' , function () { // testing populate

		scope.populate({
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false
		});

		expect(scope.toDoList).toEqual(
			[
				{
					title : "relax",
					details : "sleep",
					complete : false,
					id:1
				},
				{
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false,
				id:2
				}
			]
		);


	} );


	it('should make sure a certain task is complete' , function (){// testing togglecomplete

		scope.toDoList[999] = {
			title:"learn",
			details:"js",
			complete:false,
			id:999
		};

		scope.toggleCompletion(scope.toDoList[999]);

		expect(scope.toDoList[999].complete).toEqual(true);

		scope.toggleCompletion(scope.toDoList[999]);

		expect(scope.toDoList[999].complete).toEqual(false);

	});


	it('should delete existing task from list' , function (){

		expect(scope.toDoList).toEqual([{
					title : "relax",
					details : "sleep",
					complete : false,
					id:1
				}]);
		
		scope.deleteTask(1);// deletes task by id..
		expect(scope.toDoList).toEqual([]); /// at this point, todo list must be an empty array

	});

	it('should add new type of task to list and observe values' , function (){

		scope.populate(
			{
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false
			}
		);

		expect(scope.toDoList).toEqual([
		{
					title : "relax",
					details : "sleep",
					complete : false,
					id:1

		},
		{
			title:"Learn Angular",
			category:"self study",
			details:"Do TDD karma",
			complete : false,
			id:2
		}



		]);

	});

	// validationtests

	it('throws error when input is Null' , function (){

		expect( function (){

			scope.populate(

				{
					title:null,
					category:"self study",
					details:"Do TDD karma"
				}

			);

		}).toThrow(new Error("Input cannot be Null, Empty or Undefined"));

	});


	it('throws error when input is undefined' , function (){

		expect( function (){

			scope.populate(

				{
					title:undefined,
					category:"self study",
					details:"Do TDD karma"
				}

			);

		}).toThrow(new Error("Input cannot be Null, Empty or Undefined"));
		
	});


	it('throws error when input is empty' , function (){

		expect( function (){

			scope.populate(

				{
					title:"",
					category:"self study",
					details:"Do TDD karma"
				}

			);

		}).toThrow(new Error("Input cannot be Null, Empty or Undefined"));
		
	});



	it('recognize no deadline day' , function (){

		scope.populate(
			{
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false
			}
		);

		expect(scope.toDoList).toEqual([
		{
					title : "relax",
					details : "sleep",
					complete : false,
					id:1

		},
		{
			title:"Learn Angular",
			category:"self study",
			details:"Do TDD karma",
			complete : false,
			id:2
		}



		]);

	});



	it('Tests that the edit task function works' , function (){

			scope.populate(
			{
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false
			}
		);

		scope.editTask(scope.toDoList[1] , "New Title" , "New Category" , "New Details");

		expect(scope.toDoList[1]).toEqual(

			{
						
				title:"New Title",
				category:"New Category",
				details:"New Details",
				complete : false,
				id:2
		
			}

		);

	});



	it('Tests that the edit task function can throw errors upon null, undefined or empty inputs' , function (){

		scope.populate(
			{
				title:"Learn Angular",
				category:"self study",
				details:"Do TDD karma",
				complete : false
			}
		);

		expect( function () {
			scope.editTask(scope.toDoList[1] , null , "New Category" , "New Details");
		}).toThrow(new Error("Input cannot be Null, Empty or Undefined"));


	});

	

});