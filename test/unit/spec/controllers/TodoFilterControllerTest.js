"use strict";

describe('Controller : TodoFilterController' , function (){

	beforeEach(function () {

		module('MyToDoList');

		module(function($provide) { // assign mock service
			$provide.service('thisToDoService', mockToDoDataService);
		});

	});

	var TodolistController , scope;

	// init controller and (mock) scope
	beforeEach(inject(function ($controller, $rootScope, _thisToDoService_ ){
		scope = $rootScope.$new();
		TodolistController = $controller('TodoFilterController' , {
			$scope: scope,
			thisToDoService : _thisToDoService_
		});

	}));

	


});