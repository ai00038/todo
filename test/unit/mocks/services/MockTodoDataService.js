/**
	Mock Todo Data Service
*/

var mockToDoDataService = function () {
	var todoList = {

		latestId:1,
		tasks : {

			1 : {

				
				title : "relax",
				details : "sleep",
				complete : false,
				id : 1 

			}

		}

	};

	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllTasks : function () { // return all objs
			return objectToArray(todoList.tasks);
		},

		add : function (task) {
			var id = todoList.latestId + 1; // increment latestId
			todoList.latestId = id; // update incremented id
			task.id = id; // give task latest id
			todoList.tasks[id] = task;
			
			return id;
		},

		update : function (task) {
			todoList.tasks[task.id] = task;
		},

		remove : function (id) {// deletes from backend list?
			delete todoList.tasks[id];
		},

		getById : function (id) {
			return todoList.tasks[1];
		}
	}
};